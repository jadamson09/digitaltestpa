package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var (
	router *gin.Engine
	client *mongo.Client
)

const (
	mongoURI = "mongodb+srv://jadamson382:PanamaTigo23@testdb.dvyq5ea.mongodb.net/testdb"
	port     = "8099"
)

type Task struct {
	ID    primitive.ObjectID `json:"id" bson:"_id"`
	Title string             `json:"title" bson:"title"`
}

func init() {
    // Configurar cliente de MongoDB
    clientOptions := options.Client().ApplyURI(mongoURI)
    ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
    defer cancel()
    
    var err error
    client, err = mongo.Connect(ctx, clientOptions)
    if err != nil {
        log.Fatal("Error conectando a MongoDB:", err)
    }

    // Verificar la conexión a MongoDB
    err = client.Ping(ctx, nil)
    if err != nil {
        log.Fatal("Error conectando a MongoDB:", err)
    }

    fmt.Println("Conexión a MongoDB exitosa")

    // Configurar el enrutador Gin
    router = gin.Default()
    router.GET("/api/tasks", getTasks)
    router.POST("/api/tasks", createTask)
	router.GET("/api/test", sayHi)
}

func main() {
    defer func() {
        if client != nil {
            client.Disconnect(context.Background())
        }
    }()

    // Iniciar el servidor
    err := router.Run(":" + port)
    if err != nil {
        log.Fatal("Error iniciando el servidor:", err)
    }
}

func getTasks(c *gin.Context) {
    collection := client.Database("tasksdb").Collection("tasks")
    ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
    defer cancel()

    cursor, err := collection.Find(ctx, bson.M{})
    if err != nil {
        c.JSON(http.StatusInternalServerError, gin.H{"error": "Error al obtener tareas"})
        return
    }
    defer cursor.Close(ctx)

    var tasks []Task
    if err := cursor.All(ctx, &tasks); err != nil {
        c.JSON(http.StatusInternalServerError, gin.H{"error": "Error al decodificar tareas"})
        return
    }

    c.JSON(http.StatusOK, tasks)
}

func createTask(c *gin.Context) {
    var task Task
    if err := c.ShouldBindJSON(&task); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Formato de tarea inválido"})
        return
    }

    task.ID = primitive.NewObjectID()

    collection := client.Database("tasksdb").Collection("tasks")
    ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
    defer cancel()

    _, err := collection.InsertOne(ctx, task)
    if err != nil {
        c.JSON(http.StatusInternalServerError, gin.H{"error": "Error al crear tarea"})
        return
    }

    c.JSON(http.StatusCreated, task)
}

func sayHi(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"Hello": "World Jonathan y Luis",
	})
}